from binance.client import Client
import json
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

client = Client(config['binance']['APIKey'], config['binance']['APISecret'])

class BinanceWrapper:
        
    all_prices = {}
    current_prices_dict = {}

    def refresh_prices(self, client):
        self.all_prices = client.get_all_tickers()
        self.current_prices_dict = dict((p['symbol'], p) for p in self.all_prices)

    def get_value_traded_to_btc(self, amount, currency):
        symbol = currency + 'BTC'
        if symbol in self.current_prices_dict:
            current_price = float(self.current_prices_dict[symbol]['price'])
            return amount * current_price
        return 0

    def get_total_value(self):    
        self.refresh_prices(client)

        info = client.get_account()
        balances = info['balances']
        
        #asset_details = client.get_asset_details()

        bitcoin_total = 0

        for balance in balances:
            asset = balance['asset']
            amount_free = float(balance['free'])
            amount_locked = float(balance['locked'])
            total = amount_free + amount_locked

            if total > 0:
                bitcoin_total = self.get_value_traded_to_btc(total, asset) + bitcoin_total

#        print('Total when traded out to Bitcoin: {}'.format(bitcoin_total))
        return bitcoin_total
