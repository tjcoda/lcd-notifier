import asyncio
import cryptocom.exchange as cro
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

api_key = config['cryptocom']['APIKey']
api_secret = config['cryptocom']['APISecret']

loop = asyncio.get_event_loop()

class CryptoComWrapper:
    
    all_prices = {}
    current_prices_dict = {}

    def refresh_prices(self):
        exchange = cro.Exchange()
        self.all_prices = loop.run_until_complete(exchange.get_tickers())         
        self.current_prices_dict = dict((pair.name, self.all_prices[pair]) for pair in self.all_prices)        

    def get_value_traded_to_btc(self, amount, currency):
        symbol = '{}_BTC'.format(currency)
        if symbol in self.current_prices_dict:
            sell_price = float(self.current_prices_dict[symbol].sell_price)
            return amount * sell_price
        return 0

    def get_total_value(self):
        self.refresh_prices()

        account = cro.Account(api_key=api_key, api_secret=api_secret)
        balances = loop.run_until_complete(account.get_balance())

        bitcoin_total = 0

        for coin in balances:
            balance = balances[coin]

            if balance.total > 0:
                name = coin.name
                bitcoin_total = self.get_value_traded_to_btc(balance.total, name) + bitcoin_total

        return bitcoin_total


