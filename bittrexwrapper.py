from bittrex.bittrex import Bittrex
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

api_key = config['bittrex']['APIKey']
api_secret = config['bittrex']['APISecret']

my_bittrex = Bittrex(api_key, api_secret)

class BittrexWrapper:
    
    def get_value_traded_to_btc(self, amount, currency):        
        market = my_bittrex.get_ticker('BTC-{}'.format(currency))
        
        #if market['success'] == True:
            #print(currency)
            #print(market)
            #current_price = float(self.current_prices_dict[symbol]['price'])
            #return amount * current_price
        return 0

    def get_total_value(self):        
        bitcoin_total = 0
        balances = my_bittrex.get_balances()['result']
        for balance in balances:
            asset = balance['Currency']
            amount = balance['Balance']

            if asset == 'BTC':
                bitcoin_total = amount + bitcoin_total
            elif amount > 0:
                bitcoin_total = self.get_value_traded_to_btc(amount, asset) + bitcoin_total        

        return bitcoin_total



