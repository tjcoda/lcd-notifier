import cbpro
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

passphrase = config['coinbasepro']['Passphrase']
api_key = config['coinbasepro']['APIKey']
api_secret = config['coinbasepro']['APISecret']

auth_client = cbpro.AuthenticatedClient(api_key, api_secret, passphrase)
public_client = cbpro.PublicClient()

class CoinbaseProWrapper:

    def get_value_traded_to_btc(self, amount, currency):    
        product_name = '{}-BTC'.format(currency)    
        product_ticker = public_client.get_product_ticker(product_name)
        
        if 'message' in product_ticker and product_ticker['message'] == 'NotFound':
            print('Product {} not found on exchange CoinbasePro'.format(currency))
            return 0

        price = float(product_ticker['price'])
        btc_amount = price * amount
#        print('{} in BTC is {}'.format(product_name, btc_amount))
        return btc_amount

    def get_total_value(self):        
        accounts = auth_client.get_accounts()

 #       print('accounts : {}'.format(accounts))
        bitcoin_total = 0

        for account in accounts:            
            balance = float(account['balance'])
            currency = account['currency']

            if balance > 0:
                if currency == 'BTC':
                    bitcoin_total = balance + bitcoin_total
                else:
                    bitcoin_total = self.get_value_traded_to_btc(balance, currency) + bitcoin_total
        return bitcoin_total

    def get_1_btc_in_gbp_value(self):        
        gbp_ticker = public_client.get_product_ticker(product_id='BTC-GBP')
        
        price = float(gbp_ticker['price'])

        return price
