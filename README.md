# LCD Notifier

Please ensure that any API credentials you generate to allow monitoring of finance do not have permission to make trades or withdrawals, read-only access.

## Install

`pip install -r requirements.txt`