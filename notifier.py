from gpiozero import LED
import time
from i2c_lcd import lcd
from time import *

import datetime
import binancewrapper
import bittrexwrapper
import coinbaseprowrapper
import cryptocomwrapper

flashing=True
LED_ON=False

occasional_update_counter = 0

display=['','','','']

notifications = []

led = LED(18)
mylcd = lcd()

datapoint = {}

def alertFromTotal(total):
    return total < 2000 or total > 2500

def fetchCrypto():
    global datapoint
    datapoint['total_binance'] = binancewrapper.BinanceWrapper().get_total_value()
    datapoint['total_bittrex'] = bittrexwrapper.BittrexWrapper().get_total_value()
    datapoint['total_coinbasepro'] = coinbaseprowrapper.CoinbaseProWrapper().get_total_value()
    datapoint['total_cryptocom'] = cryptocomwrapper.CryptoComWrapper().get_total_value()    
    datapoint['total'] = datapoint['total_binance'] + datapoint['total_bittrex'] + datapoint['total_coinbasepro'] + datapoint['total_cryptocom']

def formatForLine(value):
    return value[0:20]

def checkForNotifications():
    global notifications
    if len(notifications) == 0:
        notifications.append("Slack: John Smith")
        notifications.append("Email: [Change Request]")

def setLED(value):
    global LED_ON, led

    if LED_ON == value:
        return

    if value:
        led.on()
    else:
        led.off()
    if not value == display[index-1]:        
        mylcd.lcd_display_string(value, index)
        display[index-1] = value

def updateDisplay():
    global notifications
    if len(notifications) > 0:
        setDisplayLine(formatForLine(notifications[0]), 1)
    if len(notifications) > 1:
        setDisplayLine(formatForLine(notifications[1]), 2)
    if len(notifications) > 2:
        setDisplayLine(formatForLine(notifications[2]), 3)
    if len(notifications) > 3:
        setDisplayLine(formatForLine(notifications[3]), 4)

def perform_infrequent_updates():
    global flashing
    fetchCrypto()
    gbp_conversion = coinbaseprowrapper.CoinbaseProWrapper().get_1_btc_in_gbp_value()
    current_total_gbp = round(gbp_conversion * datapoint['total'], 2)
    notifications[1] = "£{}".format(current_total_gbp)
    flashing = alertFromTotal(current_total_gbp)

while True:
#    checkForNotifications()

    occasional_update_counter = occasional_update_counter + 1
    if occasional_update_counter > 90:
        occasional_update_counter = 0
        perform_infrequent_updates()

    if len(notifications) == 0:
        notifications.append('')
        notifications.append('')

    notifications[0] = strftime("%H:%M:%S", gmtime())

    updateDisplay()

    if flashing > 0:
        setLED(not LED_ON)
    else:
        setLED(False)

    sleep(0.3)

